import logo from './logo.svg';
import './App.css';
import Navigation from './components/Navigation';
import Header from './components/Header';
import About from './components/About';
import Services from './components/Services';
import Callout from './components/Callout';
import Portfolio from './components/Portfolio';
import CallToAction from './components/CallToAction';
import Map from './components/Map';
import Footer from './components/Footer';
import ContactUs from './components/ContactUs';

function App() {
  return (
    <div>
      <Navigation/>
      <Header/>
      <Portfolio/>
      <About/>
      <Callout/>
      <ContactUs/>
      <Footer/>
    </div>
  );
}

export default App; 
