import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css'; 
import 'slick-carousel/slick/slick-theme.css';
const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
};
const portfolioItem = [
    {
        img: 'assets/img/aslec-home.JPG',
        header: 'Landing Page for a Health Care Provider Client.',
        caption: 'This is a part of a complete suite of services made by the team for the client.',
        techStack: ['js', 'html5', 'css'],
        link: 'https://aslechomecaregroup.com/',
    },
    {
        img: 'assets/img/aslec-dashboard.JPG',
        header: 'Admin Dashboard for a Health Care Provider Client.',
        caption: 'Admin Dashboard complete with authentication, employee database, inquiry and onboarding flow.',
        techStack: ['js', 'html5', 'css', 'nodejs', 'mongodb'],
        link: 'https://admin.aslechomecaregroup.com/',
    },
    {
        img: 'assets/img/scantoolgroup-pim.JPG',
        header: 'Product Page for a Multinational Supplier of Industrial Machines',
        caption: 'A CMS driven webpage that features multiple brands each with its own product offerings. The page also features internationalization to cater multilingual clients.',
        techStack: ['php', 'pimcore', 'mysql', 'js', 'html5', 'css'],
        link: 'https://scantoolgroup.dk/',
    }, 
];

export default function Portfolio() {
    return (
        <div>
            {/* Portfolio*/}
            <section className="content-section nxst-yellow-bg" id="portfolio">
                <div className="container px-4 px-lg-5">
                    <div className="content-section-heading">
                        <h2 className="mb-5">Projects</h2>
                        <h4 className="mx-auto mb-5">
                            Our growing number of our personal and client projects.
                        </h4>
                    </div>
                    <div className="row gx-0">
                        <Slider {...settings}>
                            {portfolioItem.map(
                                (portfolioItem, index) => (
                                    <div className="col-lg-6" key={index.toString()}>
                                        <div className="portfolio-item">
                                            <div className="caption">
                                                <div className="caption-content">
                                                    <div className="h1">{portfolioItem.header}</div>
                                                    <p className="web-only mb-3">{portfolioItem.caption}</p>
                                                    <div className="web-only container">
                                                        <ul className="list-inline">
                                                            {portfolioItem.techStack.map(
                                                                tech => (
                                                                    <li className="list-inline-item ml-1">
                                                                        <img className="img-techstack" src={`assets/img/techstacks/${tech}.png`}></img>
                                                                    </li>
                                                                )
                                                            )}
                                                        </ul>
                                                        <p className="text-muted small mb-0">Copyright © Nexstrive 2021</p>
                                                    </div>
                                                    <a className="mb-1" href={portfolioItem.link} target="_blank">View <i className="fas fa-external-link-alt"></i></a>
                                                </div>
                                            </div>
                                            <img className="img-portfolio" src={portfolioItem.img} alt="..." />
                                        </div>
                                    </div>
                                )
                            )}
                        </Slider>
                    </div>
                </div>
            </section>
        </div>
    )
}
