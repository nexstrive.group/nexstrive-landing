import React from 'react'

export default function Callout() {
    return (
        <div>
            <section className="callout" id="callout">
                <div className="container px-4 px-lg-5 text-center">
                    <h2 className="mx-auto mb-5">
                        We have over 5 years of industry experience working with top companies and various clients.
                    </h2>
                    <div className="row mt-6 mb-5">
                        <h4 className="mx-auto mb-5">
                            We can help you with:
                        </h4>
                    </div>
                    <div className="row">
                        <div className="col-sm-2 contact-email mt-5">
                            <i className="h1 far fa-window-restore"></i>
                            <h4 className="mx-auto mb-5">
                                Websites
                            </h4>
                        </div>
                        <div className="col-sm-2 contact-email mt-5">
                            <i className="h1 far fa-play-circle"></i>
                            <h4 className="mx-auto mb-5">
                                Tube Sites
                            </h4>
                        </div>
                        <div className="col-sm-2 contact-email mt-5">
                            <i className="h1 fas fa-laptop-code"></i>
                            <h4 className="mx-auto mb-5">
                                Backend Codes
                            </h4>
                        </div>
                        <div className="col-sm-2 contact-email mt-5">
                            <i className="h1 fas fa-mobile-alt"></i>
                            <h4 className="mx-auto mb-5">
                                Mobile
                            </h4>
                        </div>
                        <div className="col-sm-2 contact-email mt-5">
                            <i className="h1 fas fa-shopping-cart"></i>
                            <h4 className="mx-auto mb-5">
                                E-Commerce Sites
                            </h4>
                        </div>
                        <div className="col-sm-2 contact-email mt-5">
                            <i className="h1 fab fa-js-square"></i>
                            <h4 className="mx-auto mb-5">
                                Javascript Projects
                            </h4>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}
