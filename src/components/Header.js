import React from 'react'

export default function Header() {
    return (
        <div>
            {/* Header*/}
            <header className="masthead d-flex align-items-center">
                <div className="container px-4 px-lg-5 text-center">
                    <span><img className="img-50-perc" src="assets/img/nexstrive-label-full.svg" alt="..." /></span>
                    <h3 className="mb-5 nxst-slogan">Digital consultants and developers</h3>
                    {/* <a className="btn btn-primary btn-xl" href="#portfolio">Find Out More</a>    */}
                </div>
            </header>
        </div>
    )
}
