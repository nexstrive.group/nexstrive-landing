import React from 'react'

export default function About() {
    return (
        <div>
            {/* About*/}
            <section className="content-section bg-light" id="about">
                <div className="container px-4 px-lg-5 text-center mb-5">
                    <div className="row gx-4 gx-lg-5 justify-content-center">
                        <div className="col-lg-12">
                            <div className="content-section-heading">
                                <h2 className="mb-5">About The Team</h2>
                                <h4 className="mx-auto mb-5">
                                    Meet the people behind Nexstrive
                                </h4>
                            </div>
                            <div className="row">
                                <div className="col-sm-4">
                                    <img className="team_avatar" src="assets/img/nikko-avatar.png" alt="team member pic"></img>
                                    <div className="h2 mb-2">Nikko</div>
                                    <div className="follow-us_social-links">
                                        <a href="https://www.linkedin.com/in/nikko-canlas-53890791/" target="_blank">
                                            <i className="fab fa-linkedin-in"></i>
                                        </a>
                                    </div>
                                    <div className="follow-us_social-links">
                                        <a href="https://www.facebook.com/canlas.nikko" target="_blank">
                                            <i className="fab fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div className="follow-us_social-links">
                                        <a href="https://github.com/nikkonexstrive" target="_blank">
                                            <i className="fab fa-github"></i>
                                        </a>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <img className="team_avatar" src="assets/img/louj-avatar.png" alt="team member pic"></img>
                                    <div className="h2 mb-2">Louj</div>
                                    <div className="follow-us_social-links">
                                        <a href="https://www.linkedin.com/in/lourdes-jeanne-virata-0b878327/" target="_blank">
                                            <i className="fab fa-linkedin-in"></i>
                                        </a>
                                    </div>
                                    <div className="follow-us_social-links">
                                        <a href="https://www.facebook.com/loujvirata" target="_blank">
                                            <i className="fab fa-facebook"></i>
                                        </a>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <img className="team_avatar" src="assets/img/carlo-avatar.png" alt="team member pic"></img>
                                    <div className="h2 mb-2">Carlo</div>
                                    <div className="follow-us_social-links">
                                        <a href="https://www.linkedin.com/in/carloadamos/" target="_blank">
                                            <i className="fab fa-linkedin-in"></i>
                                        </a>
                                    </div>
                                    <div className="follow-us_social-links">
                                        <a href="https://www.facebook.com/john.adamos" target="_blank">
                                            <i className="fab fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div className="follow-us_social-links">
                                        <a href="https://github.com/carlonexstrive" target="_blank">
                                            <i className="fab fa-github"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6">
                                    <img className="team_avatar" src="assets/img/mier-avatar.png" alt="team member pic"></img>
                                    <div className="h2 mb-2">Mier</div>
                                    <div className="follow-us_social-links">
                                        <a href="https://www.linkedin.com/in/smier4u/" target="_blank">
                                            <i className="fab fa-linkedin-in"></i>
                                        </a>
                                    </div>
                                    <div className="follow-us_social-links">
                                        <a href="https://www.facebook.com/s.mier4u" target="_blank">
                                            <i className="fab fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div className="follow-us_social-links">
                                        <a href="#" target="_blank">
                                            <i className="fab fa-github"></i>
                                        </a>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <img className="team_avatar" src="assets/img/bert-avatar.png" alt="team member pic"></img>
                                    <div className="h2 mb-2">Robert</div>
                                    <div className="follow-us_social-links">
                                        <a href="https://www.linkedin.com/in/robertchristianobias/" target="_blank">
                                            <i className="fab fa-linkedin-in"></i>
                                        </a>
                                    </div>
                                    <div className="follow-us_social-links">
                                        <a href="https://www.facebook.com/rchristianobias" target="_blank">
                                            <i className="fab fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div className="follow-us_social-links">
                                        <a href="https://github.com/robertnexstrive" target="_blank">
                                            <i className="fab fa-github"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}
