import React from 'react'

export default function Footer() {
    return (
        <div>
            {/* Footer*/}
            <footer className="footer text-center">
                <div className="container px-4 px-lg-5">
                    <ul className="list-inline mb-5">
                        <li className="list-inline-item">
                            <a className="social-link rounded-circle text-white mr-3" href="https://www.facebook.com/groups/3015059005375930" target="_blank">
                                <i className="icon-social-facebook" />
                            </a>
                        </li>
                        {/* <li className="list-inline-item">
                            <a className="social-link rounded-circle text-white mr-3" href="#!"><i className="icon-social-twitter" /></a>
                        </li> */}
                        <li className="list-inline-item">
                            <a className="social-link rounded-circle text-white" href="https://github.com/Nexstrive" target="_blank">
                                <i className="icon-social-github" />
                            </a>
                        </li>
                    </ul>
                    <p className="text-muted small mb-0">Copyright © Nexstrive 2021</p>
                </div>
            </footer>
            {/* Scroll to Top Button*/}
            <a className="scroll-to-top rounded" href="#page-top"><i className="fas fa-angle-up" /></a>
        </div>
    )
}
