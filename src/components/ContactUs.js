import React from 'react'

export default function ContactUs() {
    return (
        <div>
            {/* Contact Us */}
            <section className="content-section nxst-yellow-bg contact-us" id="contact">
                <div className="container px-4 px-lg-5 text-center">
                    <div className="content-section-heading">
                        <h2 className="contact-us-bold mb-5">Contact Us</h2>
                        <h4 className="mx-auto mb-5">
                            We are always looking for new projects and clients. Let's have a chat on how we can help make your ideas a reality!
                        </h4>
                    </div>
                    <div className="row">
                        <div className="col-lg-4 contact-email mt-5">
                            <i className="h1 far fa-envelope"></i>
                            <h4 className="mx-auto mb-5">
                                business@nexstrive.com
                            </h4>
                        </div>
                        <div className="col-lg-4 contact-email mt-5">
                            <i className="h1 far fa-comment-alt"></i>
                            <h4 className="mx-auto mb-5">
                                +63 949 814 7432
                            </h4>
                        </div>
                        <div className="col-lg-4 contact-email mt-5">
                            <i className="h1 far fa-compass"></i>
                            <h4 className="mx-auto mb-5">
                                We are located in the Philippines!
                            </h4>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}
